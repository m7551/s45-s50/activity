import {Fragment} from 'react';
import Banner from '../components/Banner';
import Highlights from '../components/Highlights';

export default function Home(){

    const data = {
        title: "Zuitt Coding Program",
        sub: "Opportunities for everyone, everywhere",
        label: "Enroll Now",
        destination: "/courses"
    }
    
    return(
        <Fragment>
            <Banner bannerProp={data} />
            <Highlights/>
        </Fragment>
    )
}