import {useState, useEffect, useContext} from 'react';
import {Container, Card, Row, Col, Button} from 'react-bootstrap';

import {useParams, useNavigate, Link} from 'react-router-dom';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';

export default function CourseView(){
    
    const {user} = useContext(UserContext);

    const navigate = useNavigate();
    // useParams() allows to retrieve the courseId passed via the URL
    const {courseId} = useParams()
    const [name, setName] = useState("");
    const [description, setDescription] = useState("");
    const [price, setPrice] = useState(0);

    // Function for enrolling a user
    const enroll = (courseId) => {
        fetch('http://localhost:4000/users/enroll', {
            method: "POST",
            headers: {
                'Content-type': 'application/json',
                Authorization: `Bearer ${localStorage.getItem('token')}`
            },
            body: JSON.stringify({
                courseId: courseId
            })
        })
        .then(res => res.json())
        .then(data => {
            // console.log(data)
            if(data === true){
                Swal.fire({
                    title: "Congratulations!",
                    text: "Your are now enrolled in this course",
                    icon: "success"
                })
                navigate("/courses")
            } else {
                Swal.fire({
                    title: "Something went wrong!",
                    text: "Please try again.",
                    icon: "error"
                })
            }
        })
    }

    useEffect(() => {
        console.log(courseId)

        fetch(`http://localhost:4000/courses/${courseId}`)
        .then(res => res.json())
        .then(data =>{
            // console.log(data)

            setName(data.name);
            setDescription(data.description);
            setPrice(data.price);
        })
    },[courseId])

    
    return(
        <Container className="mt-5">
            <Row>
                <Col lg={{span:6, offset: 3}}>
                    <Card>
                        <Card.Body className="text-center">
                            <Card.Title>{name}</Card.Title>
                            <Card.Subtitle>Description:</Card.Subtitle>
                            <Card.Text>{description}</Card.Text>
                            <Card.Subtitle>Price:</Card.Subtitle>
                            <Card.Text>{price}</Card.Text>
                            <Card.Subtitle>Class Schedule</Card.Subtitle>
                            <Card.Text>8 AM - 5 PM</Card.Text>
                            {
                                (user.id !== null) ?
                                <Button variant="primary" onClick={() => enroll(courseId)}>Enroll</Button>
                                :
                                <Link className="btn btn-primary" to="/login">Login to Enroll</Link>
                            }
                        </Card.Body>
                    </Card>
                </Col>
            </Row>
        </Container>
    )
}