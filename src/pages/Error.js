
import Banner from '../components/Banner';

export default function Error(){

    const data = {
        title: "404 - Page Not Found",
        sub: "The page you're looking for does not exist",
        label: "Back to Home",
        destination: "/"
    }

    return(
                <Banner bannerProp={data} />
        )

}