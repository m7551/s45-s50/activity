import PropTypes from 'prop-types';
import {Fragment, useEffect, useState} from 'react';
import CourseCard from '../components/CourseCard';
// import coursesData from '../data/coursesData';


export default function Courses(){
    // console.log(coursesData)

    // State that will be used to store the retrieved courses from the database
    const [courses, setCourses] = useState([])
    
    useEffect(() => {
        fetch('http://localhost:4000/courses')
        .then(res => res.json())
        .then(data => {
            // console.log(data)
            setCourses(data.map(course => {
                return (
            // key props set for the unique class (in this case, the _id)
                    <CourseCard key={course._id} courseProp={course}/>
                )
            }))
        })
    },[])
    
    return (
        <Fragment>
            {courses}
        </Fragment>
    )
}

// Validating the course's properties
CourseCard.propTypes = {
    courseProp: PropTypes.shape({
        name: PropTypes.string.isRequired,
        description: PropTypes.string.isRequired,
        price: PropTypes.number.isRequired
    })
}