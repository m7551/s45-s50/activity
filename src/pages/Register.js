import {useState, useEffect, useContext} from 'react';
import {Navigate, useNavigate} from "react-router-dom";
import {Form, Button, Container } from 'react-bootstrap';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';

export default function Register(){
    const navigate = useNavigate();

    // Access the 'user' state from Parent Component
    const {user} = useContext(UserContext)
    const [firstName, setFirstName] = useState('');
    const [lastName, setLastName] = useState('');
    const [email, setEmail] = useState('');
    const [mobileNo, setMobileNo] = useState('');
    const [password, setPassword] = useState('');
    const [password2, setPassword2] = useState('');
    const [isActive, setIsActive] = useState(false)

    // console.log(email)
    // console.log(password1)
    // console.log(password2)

    function registerUser(e){
        e.preventDefault()

        // Will fetch the CheckEmail controller from the backend first
        fetch('http://localhost:4000/users/checkEmail', {
            method: "POST",
            headers: {
                'Content-type': 'application/json'
            },
            body: JSON.stringify({
                email: email
            })
        })
        // If data is false (No duplicate email), will call the uniqueEmail function (line 58). Else (duplicate email found), will alert using swal
        .then(res => res.json())
        .then(data => {
            // console.log(`Data: ${data}`)
            if(!data){
                uniqueEmail()
                // console.log(data)
            } else{
                Swal.fire({
                    title: 'Duplicate email found',
                    text: 'Please provide another email',
                    icon: 'error'
                })
                
                setPassword('');
                setPassword2('');
            }
        })
    }

    const uniqueEmail = () => {
        // Fetch the register controller from the backend
        fetch('http://localhost:4000/users/register', {
            method: "POST",
            headers: {
                'Content-type': 'application/json'
            },
            body: JSON.stringify({
                firstName: firstName,
                lastName: lastName,
                email: email,
                mobileNo: mobileNo,
                password: password
            })
        })
        .then(res => res.json())
        .then(data => {
            if(data){
                Swal.fire({
                    title: 'Registration Successful',
                    text: `Welcome to Zuitt, ${firstName} ${lastName}!`,
                    icon: 'success'
                })
                navigate("/login")
            } else {
                Swal.fire({
                    title: "Registratoin failed",
                    icon: "error",
                    text: "Please try again."
                })
            }
        })
    }
    
    
    // Effect hook for validation
    useEffect(() => {
        // No blank fields
        // Mobile number should be 11 characters
        // Password must match
        if((firstName !=='' && lastName !=='' && email !== '' && password !== '' && password2 !== '' && mobileNo !== '') && (mobileNo.length === 11) && (password === password2)){
            setIsActive(true);
        } else {
            setIsActive(false)
        }
    }, [firstName, lastName, email, password, password2, mobileNo])

    // const stored = localStorage.getItem('token');
    // console.log(`Stored: ${stored}`)
    return (
        // If there's an email stored in local storage, redirect to /courses
        (user.id !== null) ?
        <Navigate to="/courses"/>
        :
        <Container className="mt-3">
            <Form onSubmit = {(e) => registerUser(e)}>
                <Form.Group className="mb-3" controlId="userEmail">
                    <Form.Label>First Name</Form.Label>
                    <Form.Control 
                            value = {firstName} 
                            onChange = {e => setFirstName(e.target.value)} 
                            type="text" 
                            placeholder="Enter your first name" 
                            required />
                </Form.Group>

                <Form.Group className="mb-3" controlId="userEmail">
                    <Form.Label>Last Name</Form.Label>
                    <Form.Control 
                            value = {lastName} 
                            onChange = {e => setLastName(e.target.value)} 
                            type="text" 
                            placeholder="Enter your last name" 
                            required />
                </Form.Group>

                <Form.Group className="mb-3" controlId="userEmail">
                    <Form.Label>Email address</Form.Label>
                    <Form.Control 
                            value = {email} 
                            onChange = {e => setEmail(e.target.value)} 
                            type="email" 
                            placeholder="Enter email" 
                            required />
                </Form.Group>

                <Form.Group className="mb-3" controlId="password2">
                    <Form.Label>Mobile No</Form.Label>
                    <Form.Control
                            value = {mobileNo}
                            onChange = {e => setMobileNo(e.target.value)} 
                            type="text" 
                            placeholder="Enter your mobile number" 
                            required />
                </Form.Group>

                <Form.Group className="mb-3" controlId="password1">
                    <Form.Label>Password</Form.Label>
                    <Form.Control 
                            value = {password}
                            onChange = {e => setPassword(e.target.value)}
                            type="password" 
                            placeholder="Password" 
                            required />
                </Form.Group>
                <Form.Group className="mb-3" controlId="password1">
                    <Form.Label>Verify your password</Form.Label>
                    <Form.Control 
                            value = {password2}
                            onChange = {e => setPassword2(e.target.value)}
                            type="password" 
                            placeholder="Password" 
                            required />
                </Form.Group>


                {
                    isActive ? 
                    <Button variant="primary" type="submit" id="submitBtn">
                    Submit
                    </Button>
                    :
                    <Button variant="primary" type="submit" id="submitBtn" disabled>
                    Submit
                    </Button>
                }
            </Form>
        </Container>
    )
}