import { useState, useEffect, useContext } from "react";
import {Navigate} from "react-router-dom";
import {Form, Button, Container} from 'react-bootstrap';
import UserContext from "../UserContext";
// Import Sweet Alert 2
import Swal from 'sweetalert2';

export default function Login(){

    // useContext is another Hook
    const {user, setUser} = useContext(UserContext);
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('')
    const [isActive, setIsActive] = useState(false);

    function loginUser(e) {
        // PreventDefault method prevents the page redirection via form submission
        e.preventDefault();

        // Syntax: 
        //     fetch('url', {options})
        //         .then(res => res.json())
        //         .then(data => {})

        // Will integrate the React login to our backend using fetch
        fetch('http://localhost:4000/users/login', {
            method: "POST",
            headers: {
                'Content-type': 'application/json'
            },
            body: JSON.stringify({
                email: email,
                password: password
            })
        })
        // the result should be in json in order for the backend to read the data
        .then(res => res.json())
        .then(data => {
            console.log(data)
            // If the data.access is undefined, we will set it to a property called 'token' and call retrieveUserDetails()
            if(typeof data.access !== "undefined"){
                localStorage.setItem('token', data.access)
                retrieveUserDetails(data.access)

                // Alert successful if the login was successful
                Swal.fire({
                    title: "Login Successful",
                    icon: "success",
                    text: "Welcome to Zuitt"
                })
            } else {
                Swal.fire({
                    title: "Authentication Failed",
                    icon: "error",
                    text: "Check your login details and try again"
                })
            }
        })

        // Set the email from the authenticated user in the local storage
        // Syntax:
        //     localStorage.setItem('propertyName', value)


        // Clear the input fields after logging in.
        setEmail('');
        setPassword('');
        
        // alert('You are now logged in!');
    }

    // This function use to set the user's state using the bearer token
    const retrieveUserDetails = (token) => {
        fetch('http://localhost:4000/users/details', {
            headers: {
                Authorization: `Bearer ${token}`
            }
        })
        .then(res => res.json())
        .then(data => {
            console.log(data)

            setUser({
                id: data._id,
                isAdmin: data.isAdmin
            })
        })
    }

    // Will make the button clickable if setIsActive is true (line 71-80)
    useEffect(() => {
        if(email !== '' && password !== ''){
            setIsActive(true)
        } else {
            setIsActive(false)
        }
    }, [email, password])

    
    return (
        (user.id !== null) ? 
            <Navigate to ="/courses" />
        :
            <Container>
            <Form onSubmit={(e) => loginUser(e)}>
                <Form.Group className="mb-3" controlId="userEmail">
                    <Form.Label>Email address</Form.Label>
                    <Form.Control
                    value = {email}
                    onChange = {e => setEmail(e.target.value)} 
                    type="email" 
                    placeholder="Enter email"
                    required />
                </Form.Group>

                <Form.Group className="mb-3" controlId="userPassword">
                    <Form.Label>Password</Form.Label>
                    <Form.Control 
                    value = {password}
                    onChange = {e => setPassword(e.target.value)} 
                    type="password" 
                    placeholder="Password"
                    required />
                </Form.Group>
                
                    {
                        isActive ?
                        <Button variant="primary" type="submit">
                        Submit
                        </Button>
                        :
                        <Button variant="primary" type="submit" disabled>
                        Submit
                        </Button>
                    }
                </Form>
            </Container>
    )
}