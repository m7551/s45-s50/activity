import React from 'react'
// Navigate changes the current location (formerly Redirect)
import {Navigate} from 'react-router-dom';
import { useContext, useEffect } from 'react';
import UserContext from '../UserContext'

export default function Logout(){

    // Will import the context from App.js
    const {setUser, unsetUser} = useContext(UserContext)

    unsetUser();

    //add a useEffect to run our setUser. This useEffect will have an empty dependency array.
    // 2nd param is empty array because we only need to run this one time.
    useEffect(() => {
        setUser({
            // id: null,
            // isAdmin: null
            id: null,
            isAdmin: null
        })
    }, [])

    return(
        <Navigate to="/login" />
    )
}