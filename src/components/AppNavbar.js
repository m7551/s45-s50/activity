import {useState, Fragment, useContext} from 'react'
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
import Container from 'react-bootstrap/Container';
import {Link} from 'react-router-dom';
import UserContext from '../UserContext';

export default function AppNavbar(){

    
    // const [user, setUser] = useState(localStorage.getItem("email"));
    const {user} = useContext(UserContext);

    return (
        
        <Navbar bg="light" expand="lg">
            <Container>
                <Navbar.Brand as = {Link} to = "/">Zuitt</Navbar.Brand>
                <Navbar.Toggle aria-controls="basic-navbar-nav" />
                <Navbar.Collapse id="basic-navbar-nav">
                <Nav className="ml-auto">
                    <Nav.Link as = {Link} to = "/">Home</Nav.Link>
                    <Nav.Link as = {Link} to = "/courses">Courses</Nav.Link>
                    
                    {/* Login and Register tab will disappear if the user is currently logged in. Instead, Logout will appear */}
                    {
                        (user.id !== null) ?
                        <Nav.Link as = {Link} to = "/logout">Logout</Nav.Link>

                        :
                        <Fragment>
                            <Nav.Link as = {Link} to = "/login">Login</Nav.Link>
                            <Nav.Link as = {Link} to = "/register">Register</Nav.Link>
                        </Fragment>
                    }
                </Nav>
                </Navbar.Collapse>
            </Container>
        </Navbar>
        
    )
}