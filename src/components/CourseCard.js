import { Button, Card, Container } from 'react-bootstrap';
import {Link} from 'react-router-dom';

export default function CourseCard({courseProp}){
    // console.log(courseProp)
    const { name, description, price, _id } = courseProp

    // useState Hook
    // Syntax:
    //     const [getter, setter] = useState(initialGetterValue)
    

    // useEffect() use to perform certain task everytime there's an update

    return(
        <Container className="p-3">
            <Card>
                <Card.Body>
                    <Card.Title>{name}</Card.Title>
                    <Card.Subtitle>Description</Card.Subtitle>
                    <Card.Text>
                    {description}
                    </Card.Text>
                    <Card.Subtitle>Price:</Card.Subtitle>
                    <Card.Text>
                        {price} PHP
                    </Card.Text>
                    <Link className="btn btn-primary" to={`/courses/${_id}`}>Details</Link>
                </Card.Body>
            </Card>
        </Container>
    )
}