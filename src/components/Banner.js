// import Button from 'react-bootstrap/Button';
// import Row from 'react-bootstrap/Row';
// import Col from 'react-bootstrap/Col';
// OR
import { Button, Row, Col, Container } from 'react-bootstrap';
import {Link} from 'react-router-dom'

export default function Banner({bannerProp}){
    console.log(bannerProp)

    const { title, sub, label, destination} = bannerProp

    return(
        <Container>

                <Row>
                    <Col className="p-5">
                        <h1>{title}</h1>
                        <p>{sub}</p>
                        <Button variant="primary" as={Link} to={destination}>{label}</Button>
                    </Col>
                </Row>
        </Container>
    )
}