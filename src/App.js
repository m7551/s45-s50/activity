// Importing Order:
// Modules
// Components
// Styles
// Fragment used if there is multiple components
import {useState, useEffect} from 'react';
import {BrowserRouter as Router} from 'react-router-dom'
import {Routes, Route} from 'react-router-dom'
// Import components
import AppNavbar from './components/AppNavbar';
import Courses from './pages/Courses'
import CourseView from './pages/CourseView';
import Home from './pages/Home';
import Register from './pages/Register';
import Login from './pages/Login';
import Logout from './pages/Logout';
import Error from './pages/Error';
import {UserProvider} from './UserContext';
import './App.css';

function App() {

  // getItem() will return the value from the specified key (in this case, email). Will return null if does not exist
  const [user, setUser] = useState({
    id: null,
    isAdmin: null
  })

  // clear() will clear all key/value pairs in the local Storage
  // Function to clear the local storage for Logout
  const unsetUser = () => {
    localStorage.clear();
  }

  // Will use this hook so that whenever the user is logged in and the page was refresh, the user's details will not clear
  useEffect(() => {
    // Will fetch the user's details using the token provided when the user was logged in
    fetch("http://localhost:4000/users/details", {
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`
      }
    })
    .then(res => res.json())
    .then(data => {
      if(typeof data._id !== "undefined"){
        setUser({
          id: data._id,
          isAdmin: data.isAdmin
        })
      } else {
          setUser({
            id: null,
            isAdmin: null
          })
      }
    })

  }, [])

  return (
    // the 'user', 'setUser', and 'unsetUser' can now access by all component
    <UserProvider value = {{user, setUser, unsetUser}}>
      <Router>
        <AppNavbar />
        <Routes>
          <Route exact path = "/" element ={<Home />} />
          <Route exact path = "/courses" element ={<Courses />} />
          <Route exact path ="/courses/:courseId" element ={<CourseView />} />
          <Route exact path = "/login" element ={<Login />} />
          <Route exact path = "/logout" element ={<Logout />} />
          <Route exact path = "/register" element ={<Register />} />
          <Route exact path="*" element={<Error />}/>
        </Routes>
      </Router>
    </UserProvider>

  );
}

export default App;
